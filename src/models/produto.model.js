const mongoose = require('mongoose')

const NewSchema = new mongoose.Schema({
    "nome": String,
    "sku": String,
    "descricao": String,
    "quantidade": {type: Number, default: 1},
    "preco": {type: Number, default:1},
    "categoria": String
})

const produtos = mongoose.model('Produtos', NewSchema)

module.exports = produtos