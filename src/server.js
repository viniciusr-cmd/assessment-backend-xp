const express = require("express");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
const routes = require("./routes")
require("dotenv").config();

const app = express();
const PORT = process.env.PORT || 8080;

mongoose.connect(
  `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@cluster0.2qo9p.mongodb.net/${process.env.MONGODB_DATABASE}?retryWrites=true&w=majority`,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
),
  (err) => {
    if (err) console.log(`ERRO: ${err}`);
    console.log(`OK: DB CONNECTED SUCCESSFULLY`);
  };
  
  app.use(cors());
  
  app.use(cookieParser());
  
  app.use(express.json());
  
  app.use(routes);

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}: http://localhost:${PORT}/`);
});
