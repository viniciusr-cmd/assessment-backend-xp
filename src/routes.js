const express = require("express");

const routes = express.Router();

const Produto = require("./controllers/produto.controllers");

routes.get("/", (req, res) => {
  res.json({ message: "Hello World! Welcome to WEBJUMP API" });
});

// PRODUCT ROUTES
routes.post("/api/produtos", Produto.create);
routes.get("/api/produtos", Produto.index);
routes.get("/api/produtos/sku/:sku", Produto.getSKU);
routes.delete("/api/produtos/sku/:sku", Produto.delete);
routes.put("/api/produtos/sku/:sku", Produto.update);
module.exports = routes;
