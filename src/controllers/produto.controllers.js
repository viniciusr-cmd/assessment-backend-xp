const Produto = require("../models/produto.model");

module.exports = {
  // GET ALL ENTRIES
  async index(req, res) {
    let product = await Produto.find();
    res.json({ product });
  },
  // GET ONE SKU ENTRY
  async getSKU(req, res) {
    const { sku } = req.params;
    let product = await Produto.findOne({ sku });
    res.json({ product });
  },
  // POST ALL ENTRIES
  async create(req, res) {
    const { nome, sku, descricao, quantidade, preco, categoria } = req.body;

    let product = await Produto.findOne({ sku });

    if (!product) {
      let data = { nome, sku, descricao, quantidade, preco, categoria };
      product = await Produto.create(data);

      return res.status(201).json(product);
    } else {
      return res.status(500).json(product);
    }
  },
  // DELETE ONE SKU ENTRY
  async delete(req, res) {
    const { sku } = req.params;

    const product = await Produto.findOneAndDelete({ sku });

    return res.status(200).json(product);
  },
  // UPDATE ONE SKU ENTRY
  async update(req, res) {
    const { nome, sku, descricao, quantidade, preco, categoria } = req.body;

    const data = { nome, sku, descricao, quantidade, preco, categoria };

    const product = await Produto.findOneAndUpdate({ sku }, data, { new: true });

    res.json({ product });
  },
};
